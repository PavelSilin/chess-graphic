package com.ptl.chees.stages.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class PieceActor extends Actor {

    private float size;
    private Texture texture;
    private String pieceType;

    public PieceActor(float x, float y, float size, String pieceType) {
        this.setSize(size);
        texture = new Texture(pieceType);
        this.pieceType = pieceType;
        setX(x);
        setY(y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), size, size);
    }

    public String getPieceType() {
        return pieceType;
    }

    public void setPieceType(String pieceType) {
        this.pieceType = pieceType;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }
}
