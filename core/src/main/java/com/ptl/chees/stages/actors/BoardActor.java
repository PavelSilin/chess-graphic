package com.ptl.chees.stages.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class BoardActor extends Actor {

    private Texture texture;

    public BoardActor(float width, float height) {
        setSize(width, height);
        texture = new Texture("board.jpg");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, 0, 0, getWidth(), getHeight());
    }
}
