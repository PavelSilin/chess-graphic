package com.ptl.chees.stages.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class StartButton extends Actor {

    private Texture current;
    private final Texture unpressed;
    private final Texture pressed;

    public StartButton() {
        this.unpressed = new Texture("start_button.png");
        this.pressed = new Texture("start_button_p.png");
        this.current = unpressed;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(current, getX(), getY(), getWidth(), getHeight());
    }

    public boolean tryToPress(int screenX, int screenY) {
        boolean isTyped = isPressed(screenX, screenY);
        if (isTyped) {
            current = pressed;
        } else {
            current = unpressed;
        }
        return isTyped;
    }

    private boolean isPressed(int screenX, int screenY) {
        return screenX >= getX()
                && screenX <= getX() + getWidth()
                && screenY >= getY()
                && screenY <= getY() + getHeight();
    }


}
