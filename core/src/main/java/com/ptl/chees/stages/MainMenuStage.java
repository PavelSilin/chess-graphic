package com.ptl.chees.stages;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.ptl.chees.ChessGame;
import com.ptl.chees.stages.actors.MainMenu;
import com.ptl.chees.stages.actors.StartButton;

public class MainMenuStage extends Stage {
    private ChessGame game;

    private final MainMenu mainMenu;
    private final StartButton startButton;

    public MainMenuStage(ChessGame game) {
        this.game = game;
        mainMenu = new MainMenu(new Texture("main.jpg"));
        mainMenu.setSize(getWidth(), getHeight());
        addActor(mainMenu);
        startButton = new StartButton();
        startButton.setPosition(this.getWidth()/3, this.getHeight()/8);
        addActor(startButton);
        startButton.setSize(this.getWidth()/3, this.getHeight()/5);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        startButton.tryToPress(screenX, (int) (this.getHeight() - screenY));
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (startButton.tryToPress(screenX, (int) (this.getHeight() - screenY))) {
            game.startGame();
            return true;
        }
        return false;
    }


}
