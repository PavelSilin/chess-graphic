package com.ptl.chees.stages.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class SelectionActor extends Actor {

    private float size;
    private Texture texture;

    public SelectionActor(float size) {
        this.setSize(size);
        texture = new Texture("selection.png");
        setX(0);
        setY(0);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), size, size);
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

}
