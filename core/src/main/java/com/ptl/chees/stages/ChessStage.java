package com.ptl.chees.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.ptl.chees.ChessGame;
import com.ptl.chees.model.board.Board;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.board.TurnResult;
import com.ptl.chees.model.exceptions.CheckmateException;
import com.ptl.chees.model.exceptions.WrongBoardStateException;
import com.ptl.chees.model.exceptions.WrongTurnException;
import com.ptl.chees.stages.actors.BoardActor;
import com.ptl.chees.stages.actors.PieceActor;
import com.ptl.chees.stages.actors.SelectionActor;

public class ChessStage extends Stage {

    private static final float TURN_DURATION = 0.5f;

    private float screenSize;
    private float cellSize;
    private float pieceSize;

    private final Board boardModel;

    private final BoardActor boardView;
    private final SelectionActor selection;
    private final PieceActor[][] coordinatesToPieces;

    private Coordinates source;
    private Coordinates target;
    private ChessGame chessGame;


    public ChessStage(ChessGame chessGame) {
        this.chessGame = chessGame;
        screenSize = Math.min(getViewport().getScreenHeight(), getViewport().getScreenWidth());
        cellSize = screenSize / 10;
        //-4 just for more pretty fitting
        pieceSize = cellSize - 2;

        this.boardModel = new Board();

        this.boardView = new BoardActor(screenSize, screenSize);
        this.addActor(boardView);

        this.coordinatesToPieces = new PieceActor[Board.DIMENSION][Board.DIMENSION];
        setPieceActors();

        for (PieceActor[] pieceActorsRow : coordinatesToPieces) {
            for (PieceActor pieceActor : pieceActorsRow) {
                if (pieceActor != null) {
                    addActor(pieceActor);
                }
            }
        }

        this.selection = new SelectionActor(cellSize);
        this.addActor(selection);
        selection.setVisible(false);
    }

    public void resizeBoard(int width, int height) {
        getViewport().update(width, height);
        getViewport().apply();
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        Coordinates coordinates = calculateCoordinates(x, y);

        if (isFillOnBoard(coordinates)) {
            if (source != null) {
                target = coordinates;
            } else {
                source = coordinates;
                selection.setVisible(true);
                selection.setPosition((source.col + 1) * cellSize, (source.row + 1) * cellSize);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        Coordinates coordinates = calculateCoordinates(x, y);

        if (isFillOnBoard(coordinates)) {
            if (target != null) {
                performTurn();
                source = null;
                target = null;
                selection.setVisible(false);
                return true;
            }
        }
        return false;
    }

    private void performTurn() {
        try {
            final TurnResult turnResult = boardModel.move(source, target);
            final PieceActor sourcePiece = coordinatesToPieces[source.row][source.col];
            final PieceActor targetPiece = coordinatesToPieces[target.row][target.col];

            // swap piece actors in matrix
            coordinatesToPieces[source.row][source.col] = null;
            coordinatesToPieces[turnResult.coordinates[0].row][turnResult.coordinates[0].col] = sourcePiece;

            float xToMove = (turnResult.coordinates[0].col + 1) * cellSize;
            float yToMove = (turnResult.coordinates[0].row + 1) * cellSize;

            if (turnResult.pawnToQueen) {
                swapPawnToQueen(sourcePiece, xToMove, yToMove);
            } else {
                // move source piece actors to new position on board
                sourcePiece.addAction(
                    Actions.moveTo(xToMove, yToMove, TURN_DURATION)
                );
            }

            // remove target piece actor from board or move it if castling
            if (targetPiece != null) {
                if (turnResult.coordinates.length == 1) {
                    targetPiece.addAction(
                            Actions.sequence(Actions.delay(TURN_DURATION), Actions.removeActor()));
                } else {
                    coordinatesToPieces[turnResult.coordinates[1].row][turnResult.coordinates[1].col] = targetPiece;
                    targetPiece.addAction(
                            Actions.moveTo(
                                    (turnResult.coordinates[1].col + 1) * cellSize,
                                    (turnResult.coordinates[1].row + 1) * cellSize,
                                    TURN_DURATION
                            )
                    );
                }
            }
        } catch (WrongTurnException e) {
            //TODO message
        } catch (WrongBoardStateException e) {
            //TODO message
        } catch (CheckmateException e) {
            chessGame.moveToMainMenu();
        }
    }

    private void swapPawnToQueen(PieceActor sourcePiece, float xToMove, float yToMove) {
        sourcePiece.addAction(
                Actions.sequence(Actions.moveTo(xToMove, yToMove, TURN_DURATION), Actions.removeActor())
        );

        PieceActor queen = new PieceActor(
            xToMove, yToMove, sourcePiece.getSize(),
            sourcePiece.getPieceType().replace("pawn", "queen")
        );
        addActor(queen);

        queen.addAction(
                Actions.sequence(
                        Actions.visible(false), Actions.delay(TURN_DURATION), Actions.visible(true)
                )
        );
        coordinatesToPieces[target.row][target.col] = queen;
    }

    private boolean isFillOnBoard(Coordinates coordinates) {
        return coordinates.row >= 0 && coordinates.row < 8 && coordinates.col >= 0 && coordinates.col < 8;
    }

    private Coordinates calculateCoordinates(int x, int y) {
        int row = (int) ((getHeight() - y) / getHeight() * 10) - 1;
        int col = (int) (x / getWidth() * 10) - 1;
        return new Coordinates(row, col);
    }

    private void setPieceActors() {
        setTeam(true);
        setTeam(false);
    }

    private void setTeam(boolean colour) {
        String colourSuffix = colour ? "w" : "b";
        float frontRowCoordinate = colour ? cellSize * 7 : cellSize * 2;
        float backRowCoordinate = colour ? cellSize * 8 : cellSize;

        String[] backRowPieces = {
                "rook-" + colourSuffix + ".png",
                "knight-" + colourSuffix + ".png",
                "bishop-" + colourSuffix + ".png",
                "king-" + colourSuffix + ".png",
                "queen-" + colourSuffix + ".png",
                "bishop-" + colourSuffix + ".png",
                "knight-" + colourSuffix + ".png",
                "rook-" + colourSuffix + ".png"
        };

        for (int i = 0; i < 8; i++) {
            setPieceToCell(frontRowCoordinate, "pawn-" + colourSuffix + ".png", i);
            setPieceToCell(backRowCoordinate, backRowPieces[i], i);
        }
    }

    private void setPieceToCell(float rowCoordinate, String pieceType, int x) {
        int y = (int) ((rowCoordinate / cellSize) - 1);
        coordinatesToPieces[y][x] = new PieceActor(
                cellSize * (x + 1) + 2,
                rowCoordinate + 2,
                pieceSize,
                pieceType
        );
    }

}
