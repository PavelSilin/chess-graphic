package com.ptl.chees.model.exceptions;

public class CheckmateException extends IllegalStateException {

    public CheckmateException(String m) {
        super(m);
    }

}
