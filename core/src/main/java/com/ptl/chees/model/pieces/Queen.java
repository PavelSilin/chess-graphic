package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.exceptions.WrongTurnException;

import java.util.List;

public class Queen extends Piece {

    public Queen(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        Coordinates source = this.coordinates;
        Coordinates target = targetCell.getCoordinates();

        int dc = source.col - target.col;
        int dr = source.row - target.row;

        if (Math.abs(dc) != Math.abs(dr) && (Math.abs(dc) != 0) == (Math.abs(dr) != 0)) {
            throw new WrongTurnException("Queen: You can't do this move!");
        }
        return move(target);
    }

}
