package com.ptl.chees.model.exceptions;

public class WrongTurnException extends IllegalArgumentException {

    public WrongTurnException(String m) {
        super(m);
    }

}
