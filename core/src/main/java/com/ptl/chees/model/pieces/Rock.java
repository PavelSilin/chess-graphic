package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.exceptions.WrongTurnException;

import java.util.List;

public class Rock extends Piece implements Castleable {

    private boolean canMakeCastling;

    public Rock(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
        canMakeCastling = true;
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        Coordinates source = this.coordinates;
        Coordinates target = targetCell.getCoordinates();
        if (source.col - target.col != 0 && source.row - target.row != 0) {
            throw new WrongTurnException("Rock: You can't do this move!");
        }
        discardCastling();
        return move(target);
    }

    @Override
    public boolean isReadyForCastling() {
        return canMakeCastling;
    }

    @Override
    public void discardCastling() {
        canMakeCastling = false;
    }

}
