package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.exceptions.WrongTurnException;

import java.util.List;

public class Bishop extends Piece {

    public Bishop(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        Coordinates source = this.coordinates;
        Coordinates target = targetCell.getCoordinates();
        if (Math.abs(source.col - target.col) != Math.abs(source.row - target.row)) {
            throw new WrongTurnException("Bishop: You can't do this move!");
        }
        return move(target);
    }

}
