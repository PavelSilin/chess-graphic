package com.ptl.chees.model.pieces;

public interface Castleable {
    boolean isReadyForCastling();
    void discardCastling();
}
