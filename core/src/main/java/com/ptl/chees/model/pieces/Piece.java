package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.exceptions.WrongTurnException;

import java.util.ArrayList;
import java.util.List;

public abstract class Piece {

    //white is true
    protected boolean colour;
    protected Coordinates coordinates;

    public Piece(boolean colour, Coordinates coordinates) {
        this.colour = colour;
        this.coordinates = coordinates;
    }

    public abstract List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException;

    protected List<Coordinates> move(Coordinates target) {
        List<Coordinates> path = new ArrayList<Coordinates>();

        // directions
        int deltaRow = target.row - coordinates.row;
        int deltaCol = target.col - coordinates.col;
        int dr = deltaRow != 0 ? (deltaRow > 0 ? 1 : -1) : 0;
        int dc = deltaCol != 0 ? (deltaCol > 0 ? 1 : -1) : 0;

        if (dr == 0 && dc == 0) {
            throw new WrongTurnException(this.getClass() + ": You have to move!");
        }

        int currentRow = coordinates.row;
        int currentCol = coordinates.col;

        while (Math.abs(target.row - currentRow) != 1 && Math.abs(currentCol - target.col) != 1) {
            Coordinates step = new Coordinates(currentRow += dr, currentCol += dc);
            path.add(step);
        }
        return path;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public boolean getColour() {
        return colour;
    }

    public void setColour(boolean colour) {
        this.colour = colour;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
}
