package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import com.ptl.chees.model.exceptions.WrongTurnException;

import java.util.Collections;
import java.util.List;

public class King extends Piece implements Castleable {

    private boolean canMakeCastling;

    public King(boolean colour, Coordinates coordinates) {
        super(colour, coordinates);
        canMakeCastling = true;
    }

    @Override
    public List<Coordinates> tryMove(Cell targetCell) throws WrongTurnException {
        Coordinates target = targetCell.getCoordinates();
        int dc = target.col - coordinates.col;
        int dr = target.row - coordinates.row;

        if (Math.abs(dc) > 1 || Math.abs(dr) > 1) {
            throw new WrongTurnException("King: You can't do this move!");
        }
        discardCastling();
        return move(target);
    }

    @Override
    protected List<Coordinates> move(Coordinates target) {
        return Collections.emptyList();
    }

    @Override
    public boolean isReadyForCastling() {
        return canMakeCastling;
    }

    @Override
    public void discardCastling() {
        canMakeCastling = false;
    }
}
