package com.ptl.chees.model.exceptions;

public class WrongBoardStateException extends IllegalStateException {

    public WrongBoardStateException(String m) {
        super(m);
    }

}
