package com.ptl.chees.model.board;

public class TurnResult {

    public final Coordinates[] coordinates;
    public final boolean pawnToQueen;

    public TurnResult(Coordinates[] coordinates, boolean pawnToQueen) {
        this.coordinates = coordinates;
        this.pawnToQueen = pawnToQueen;
    }
}
