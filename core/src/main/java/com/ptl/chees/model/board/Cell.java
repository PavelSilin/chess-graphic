package com.ptl.chees.model.board;

import com.ptl.chees.model.pieces.Piece;

public class Cell {

    private Piece piece;
    private final Coordinates coordinates;

    public Cell(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }
}
