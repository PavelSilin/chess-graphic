package com.ptl.chees;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.ptl.chees.stages.ChessStage;
import com.ptl.chees.stages.MainMenuStage;

public class ChessGame extends Game {

	private Stage stage;

	@Override
	public void create () {
		stage = new MainMenuStage(this);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().setWorldSize(width, height);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void moveToMainMenu() {
		stage.dispose();
		stage = new MainMenuStage(this);
		Gdx.input.setInputProcessor(stage);
	}

	public void startGame() {
		stage.dispose();
		stage = new ChessStage(this);
		Gdx.input.setInputProcessor(stage);
	}
}
