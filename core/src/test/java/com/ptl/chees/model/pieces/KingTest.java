package com.ptl.chees.model.pieces;

import com.ptl.chees.model.board.Cell;
import com.ptl.chees.model.board.Coordinates;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class KingTest {

    @Test
    public void kingCanMakeRightMoveUp() {
        King king = new King(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 0));
        List<Coordinates> coordinates = king.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test
    public void kingCanMakeRightMoveDiagonally() {
        King king = new King(true, new Coordinates(4, 4));
        Cell targetCell = new Cell(new Coordinates(3, 3));
        List<Coordinates> coordinates = king.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test
    public void kingCanMakeRightMoveRightDown() {
        King king = new King(true, new Coordinates(8, 0));
        Cell targetCell = new Cell(new Coordinates(7, 0));
        List<Coordinates> coordinates = king.tryMove(targetCell);
        Assert.assertEquals(0, coordinates.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void queenTrowIAEIfMoveIsWrong() {
        King king = new King(true, new Coordinates(0, 0));
        Cell targetCell = new Cell(new Coordinates(1, 2));
        king.tryMove(targetCell);
    }

}
