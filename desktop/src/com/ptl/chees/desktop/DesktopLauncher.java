package com.ptl.chees.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ptl.chees.ChessGame;

import java.awt.*;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Toolkit.getDefaultToolkit().getScreenSize().height / 2;
		config.height = Toolkit.getDefaultToolkit().getScreenSize().height / 2;
		config.resizable = false;
		new LwjglApplication(new ChessGame(), config);
	}
}
